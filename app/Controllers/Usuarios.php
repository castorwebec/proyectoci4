<?php

namespace App\Controllers;
use App\Models\Usuario;

class Usuarios extends BaseController
{
    private $usuario;

    public function index()
    {
        $data["titulo"] = "Módulo de Usuarios";
        return view("Usuarios/index", $data);
    }

    public function lista()
    {
        $this->usuario = new Usuario();
        $data["lista"] = $this->usuario->BuscarRegistros();    
        return view("Usuarios/lista", $data);
    }

    public function gestionRegistro()
    {
        $id = $this->request->getPost("id");

       //Encriptar la clave
       $encrypter = \Config\Services::encrypter();
       //$encrypted_data =  bin2hex($encrypter->encrypt(123));
       $clave = bin2hex($encrypter->encrypt($this->request->getPost("clave")));

       //Preparar el registro

       $data = [
          
           "nombre" => $this->request->getPost("nombre"),
           "apellido" => $this->request->getPost("apellido"),
           "email" => $this->request->getPost("email"),
           "fecha_nacimiento" => $this->request->getPost("fechaNacimiento"),
           "usuario" => $this->request->getPost("usuario"),
           "clave" => $clave,
           "estado" => $this->request->getPost("estado")
       ];

        //Subir la imágen si se elegido una caso contrario no
        $img = $this->request->getFile('foto');

        if($img->isValid()){
            $nombre = $img->getRandomName();
            $img->move('./public/Usuarios/fotos', $nombre);
            $foto = [ "foto" => $img->getName()];
           $data = array_merge($data, $foto);
       }

       if(intval($id) > 0){
            $this->usuario = new Usuario();
            echo json_encode($this->usuario->editarRegistro($id, $data));
       }else{
            $this->usuario = new Usuario();
            echo json_encode($this->usuario->insertarRegistro($data));
       }
    }

    public function buscarRegistro(){
        $id = $this->request->getPost("id");
        $this->usuario = new Usuario();
        //Buscar el registro en el modelo 
        $registro = (array) $this->usuario->buscarRegistroPorID($id);
        //Desencriptar la clave recuperada
        $encrypter = \Config\Services::encrypter();
        $clave_db = $encrypter->decrypt(hex2bin($registro['clave']));
        //Crear el array para reemplazo con la clave desencriptada
        $clave = array('clave' => $clave_db);
        //Reemplazar la clave descifrada en el array del registro
        $registro = array_replace($registro, $clave);
        //Enviar a la vista el resultado
        echo json_encode((object)$registro);
    }

    public function eliminarRegistro()
    {
        $id = $this->request->getPost("id");
        $this->usuario = new Usuario();
        echo json_encode($this->usuario->eliminarRegistro($id));
    }
}