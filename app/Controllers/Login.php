<?php
/* Activar en entorno de desarrollo en y la base de datos
   crear la base de datos con un ejemplo generando la clave
   Poner clave de cifrado en app/config/encryption.php
   Recomendar Query Builder de la documentación ofificial
   Poner imagen de guest
*/

namespace App\Controllers;
use App\Models\Usuario;

class Login extends BaseController
{
	private $usuario;

	public function index()
	{
		//$encrypter = \Config\Services::encrypter(); 
		//$encrypted_data =  bin2hex($encrypter->encrypt(123));
		//echo $encrypted_data;
		//$decrypted_data = $encrypter->decrypt(hex2bin($encrypted_data));
		//echo $decrypted_data;

		return view("Login/index");
	}

	public function validarIngreso(){

		$emailUsuario = $this->request->getPost("emailUsuario");
		//Verificar si el ingreso es un nombre de usuario o email
		if(filter_var($emailUsuario, FILTER_VALIDATE_EMAIL)){
			//limpiar (sanitize) el email, de ser el caso para eliminar caracteres ilegales
			$email = filter_var($emailUsuario, FILTER_SANITIZE_EMAIL);
			//Buscar el usuario por email
			$this->usuario = new Usuario();
            $resultadoUsuario = $this->usuario->buscarUsuarioPorEmail($email);
		}else{
			//limpiar caracteres ilegales
			$usuario = preg_replace("/[^a-zA-Z0-9.-]/", "", $emailUsuario);
			//Buscar el usurio por nombre de usuario
			$this->usuario = new Usuario();
			$resultadoUsuario = $this->usuario->buscarUsuarioPorUsuario($usuario);
		}

		if($resultadoUsuario)
		{
			//Descifrar la clave del registro de la bdd
			$encrypter = \Config\Services::encrypter();
			$clave_db = $encrypter->decrypt(hex2bin($resultadoUsuario->clave));

			//Comparar la clave de la bdd y la ingresada
			$clave = $this->request->getPost("clave");
			if($clave == $clave_db)
			{
				//echo "claves iguales";
				$data = [
					"nombreUsuario" => $resultadoUsuario->nombre.' '.$resultadoUsuario->apellido,
					"emailUsuario" => $resultadoUsuario->email,
					"fotoUsuario" => $resultadoUsuario->foto
				];
				session()->set($data);  
                return redirect()->to(base_url().'/escritorio');   

			}else{
				$data = ['tipo' => 'danger', 'mensaje' => 'La clave es incorrecta'];
            	return view('Login\index', $data);
			}
		}else{
			$data = ['tipo' => 'danger', 'mensaje' => 'Usuario incorrecto o inactivo'];
            return view('Login\index', $data);
		}

	}

	public function cerrarSesion()
	{
		session()->destroy();
		return redirect()->to(base_url());
	}
}	