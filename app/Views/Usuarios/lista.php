<div class="table-responsive">
<?php if($lista){ ?>
  <table id="tablaDatos" class="table table-bordered table-striped">
    <thead>
      <tr>
        <th width="5%">N</th>
        <th width="45%">Nómina</th>
        <th width="20%">Email</th>
        <th width="10%">Usuario</th>
        <th width="10%">Estado</th>
        <th width="10%"></th>
      </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    <?php foreach ($lista as $dato) { ?>
      <tr>
        <td><?php echo $i; $i++; ?></td>
        <td><?php echo $dato->apellido.' '.$dato->nombre; ?></td>
        <td><?php echo $dato->email; ?></td>
        <td><?php echo $dato->usuario; ?></td>
        <td><?php echo $dato->estado; ?></td>
        <td>
        <button type="button" class="btn btn-info btn-sm" onclick="gestionRegistro(this);" data-accion="editar" data-id="<?php echo $dato->id;?>" ><i class="far fa-edit"></i></button> 

        <button type="button" class="btn btn-danger btn-sm" onclick="gestionRegistro(this);"  data-accion="eliminar"  data-id="<?php echo $dato->id; ?>" ><i class="far fa-trash-alt"></i></button>

        </td>
      </tr>
    <?php } ?>            
    </tbody>
    <tfoot>
      <tr>
        <th>N</th>
        <th>Nómina</th>
        <th>Email</th>
        <th>Usuario</th>
        <th>Estado</th>
        <th></th>
      </tr>
    </tfoot>
  </table>
<?php }else{ ?>
  <div class="alert alert-danger">
    <b>No se encontraton datos</b>
  </div>
<?php } ?>

</div>

<script type="text/javascript" src="<?php echo base_url('assets/plugins/owner/js/datatables.js'); ?>"></script>

