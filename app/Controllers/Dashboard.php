<?php

namespace App\Controllers;

class Dashboard extends BaseController
{
    public function index()
    {
        $data["titulo"] = "Bienvenid@ al sistema";
        return view("Dashboard/index", $data);
    }
}