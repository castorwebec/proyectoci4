<?php

namespace App\Models;
use CodeIgniter\Model;

class Usuario extends Model{
    
    protected $table = 'usuario';
    protected $primaryKey = 'id';
    protected $allowedFields = ['nombre', 'apellido','foto','email','usuario','clave','estado','fecha_nacimiento'];	

    public function insertarRegistro($data)
    {
        $usuario  = new Usuario;
        return $usuario->insert($data);
    }

    public function editarRegistro($id, $data)
    {
        $usuario  = new Usuario;
        return $usuario->update($id, $data);
    }

    public function eliminarRegistro($id)
    {
        $usuario  = new Usuario;
        return $usuario->delete($id);
    }
        
    public function buscarRegistroPorID($id){
        $usuario  = new Usuario;
        $resultado = $usuario->where('id', $id)->get();
        return $resultado->getResult() ? $resultado->getResult()[0] : false;
    }

    public function buscarRegistros()
    {
		$db = db_connect();
		$resultado = $db->query('select * from usuario');
		return $resultado->getResult() ? $resultado->getResult() : false;
    }

    public function buscarUsuarioPorEmail($email)
    {
        $db = db_connect();
        $builder = $db->table($this->table)->where('email', $email)->where('estado', 'A');
        $resultado = $builder->get();
        return $resultado->getResult() ? $resultado->getResult()[0] : false;
    }

    public function buscarUsuarioPorUsuario($usuario)
    {
        $db = db_connect();
        $builder = $db->table($this->table)->where('usuario', $usuario)->where('estado', 'A');
        $resultado = $builder->get();
        return $resultado->getResult() ? $resultado->getResult()[0] : false;
    }

}