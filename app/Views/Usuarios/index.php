<?= $this->extend('Views/Dashboard/escritorio'); ?>


 

<style>

.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}

</style>

<?= $this->section('contenido'); ?>
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?= $titulo; ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Usuarios</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <button class="btn btn-success" onclick="gestionRegistro(this);" data-accion="insertar"><i class="far fa-file"></i> Nuevo</button>
        </div>
        <div id="listadoDatos" class="card-body"></div>
      </div>
    </section>

    <div class="modal fade" id="modalFormulario">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="tituloModal"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
           
            <form name="formUsuario" id="formUsuario"> 
            <input type="hidden" name="id" id="id">  
              <div class="row">
                <div class="col-sm-4 text-center">
                  <div class="kv-avatar">
                      <div class="file-loading">
                          <input id="foto" name="foto" type="file">
                      </div>
                  </div>
                <div class="kv-avatar-hint">
                    <small></small>
                </div>
              </div>
        
              <div class="col-sm-8">
                
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label>Apellidos:</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                      </div>
                      <input type="text" name="apellido" id="apellido" class="form-control">
                    </div>
                    <!-- /.input group -->
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Nombres:</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                      </div>
                      <input type="text" name="nombre" id="nombre" class="form-control">
                    </div>
                    <!-- /.input group -->
                  </div>
                </div>

                <div class="row">
                  <div class="form-group col-sm-12">
                    <label>Email:</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-envelope"></i></span>
                      </div>
                      <input type="text" name="email" id="email" class="form-control">
                    </div>
                    <!-- /.input group -->
                  </div>
                </div>

                <div class="row">
                  <div class="form-group col-sm-6">
                    <label>Fecha de nacimiento:</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-birthday-cake"></i></span>
                      </div>
                      <input type="text" name="fechaNacimiento" id="fechaNacimiento" class="form-control">
                    </div>
                    <!-- /.input group -->
                  </div>
                <div class="form-group col-sm-6">
                    <label>Estado:</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                      </div>
                      <select name="estado" id="estado" class="form-control">
                        <option value="A">ACTIVO</option>
                        <option value="I">INACTIVO</option>
                      </select>
                    </div>
                    <!-- /.input group -->
                </div>
               </div>

              <div class="row">
                 <div class="form-group col-sm-6">
                    <label>Usuario:</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                      </div>
                      <input type="text" name="usuario" id="usuario" class="form-control">
                    </div>
                    <!-- /.input group -->
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Clave:</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-key"></i></span>
                      </div>
                      <input type="text" name="clave" id="clave" class="form-control">
                    </div>
                    <!-- /.input group -->
                  </div>
                </div>  
                </div>                
               </div>
              </div>
           
            
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Cancelar</button>
              <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Guardar</button>
            </div>
          </div>

          </form>  

          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


<script>
  //Variable con la ruta del avatar por defecto
  let defaultAvatar = '<img id="fotoUsuario" src="<?php echo base_url(); ?>/public/Usuarios/fotos/guest.jpg" height="100px" width="100px" alt="Usuario"><h6 class="text-muted">Clic para seleccionar la imágen</h6>';

  $("#menuAdministracion").addClass("menu-open");
  $("#linkAdministraion").addClass("active");
  $("#menuUsuarios").addClass("active");

  //Listar los regitros de la bdd  
  listarRegistros();
  //Inicializar el plugin de file input ejecutando la función iniciarFileInput(), se pasa como parámetro la variable antes declarada: defaultAvatar 
  iniciarFileInput(defaultAvatar);


  function listarRegistros(){
    $("#listadoDatos").load("<?php echo site_url('Usuarios/lista'); ?>",{}, function(responseText, statusText, xhr){
        if(statusText == "success"){
        }
        if(statusText == "error"){
          Swal.fire("Información!", "No se pudieron cargar los registros", "info"); 
        }
      });
  }

  function gestionRegistro(objeto){
    //Detectar la acción a realizar
    var accion = $(objeto).data("accion");
    //Resetear el formulario, es decir, limpiar las cajas de texto
    $("#formUsuario").trigger("reset");
    //Destruir o resetear el fileinput para refrescar el contenido
    $('#foto').fileinput('destroy');
    //Inicializar nuevamente el fileinput
    iniciarFileInput(defaultAvatar); 

    switch(accion){
      case 'insertar':
        $("#tituloModal").text("Nuevo registro");
        var idRegistro = 0;
        $("#modalFormulario").modal("show");
      break;
      case 'editar':
        $("#tituloModal").text("Editar registro");
        //Se recupera el ide del registro seleccionado
        var idRegistro = $(objeto).data("id");
        //Buscar el registro por el id obtenido
        buscarRegistro(idRegistro);
        $("#modalFormulario").modal("show");
      break;
      case 'eliminar':
        var id = $(objeto).data("id");
        Swal.fire({
        title: 'Eliminar?',
        text: "Se borrará el registro!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!'
          }).then((result) => {
            if (result.isConfirmed) {
              
              $.ajax({
                  type : 'post',
                  url  : "<?php echo site_url('Usuarios/eliminarRegistro'); ?>",
                  dataType: 'json',
                  data: {
                    id   :   id
                      }
                })  
                .done(function(data){
                  Swal.fire(
                      'Éxito!',
                      'Registro eliminado.',
                      'success'
                    )
                  })
                  .fail(function(){
                    Swal.fire(
                      'Error!',
                      'No se pudo eliminar.',
                      'error'
                    )
                  })   
                  .always(function(){
                    listarRegistros();
                  });

            }
        })
      break;
    }
  }

  function buscarRegistro(id){
    $.ajax({
      type : 'post',
      url  : "<?php echo site_url('Usuarios/buscarRegistro'); ?>",
      dataType: 'json',
      data: {
          id   :   id
          }
    })  
    .done(function(data){
      //Se asigna cada campo del registro recuperado a las cajas de texto
      $(data).each(function(i, v){
        $("#id").val(v.id);
        $("#nombre").val(v.nombre);
        $("#apellido").val(v.apellido);
        $("#fechaNacimiento").val(v.fecha_nacimiento);
        $("#email").val(v.email);
        $("#usuario").val(v.usuario);
        $("#clave").val(v.clave);
        $("#estado").val(v.estado);
        let imgURL = '<img src="<?php echo base_url(); ?>/public/Usuarios/fotos/'+v.foto+'" height="100px" width="100px" alt="Usuario">';
        //Destruir o resetear el fileinput
        $('#foto').fileinput('destroy');
        //Inicializar el fileinput para rfrescar el contenido
        iniciarFileInput(imgURL); 
      });  

    })
    .fail(function(){
      Swal.fire(
        'Error!',
        'No cargar la información.',
        'error'
      )
    })   
    .always(function(){
    });

  }


  function iniciarFileInput(defaultAvatar){
    
    $("#foto").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      showBrowse: false,
      browseOnZoneClick: true,
      removeLabel: '',
      removeIcon: '<i class="fas fa-trash"></i>',
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-2',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: defaultAvatar,
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "gif"]
    });
  
  }  


$(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      
      let formData = new FormData($('#formUsuario')[0]);
      $.ajax({
            type : 'post',
            url  : "<?php echo site_url('Usuarios/gestionRegistro'); ?>",
            dataType: 'json',
            data : formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            enctype: 'multipart/form-data'
      })  
      .done(function(data){
        Swal.fire(
          'Éxito!',
          'Registro procesado.',
          'success'
        )
      })
      .fail(function(){
        Swal.fire(
          'Error!',
          'No se pudo procesar.',
          'error'
        )
      })   
      .always(function(){
        $("#modalFormulario").modal('hide');
        listarRegistros();
        //Resetear el formulario, es decir, limpiar las cajas de texto
        $("#formUsuario").trigger("reset");
      });


    }
  });
  $('#formUsuario').validate({
    rules: {
      apellido: {
        required : true
      },
      nombre: {
        required : true
      },
      email: {
        required: true,
        email: true,
      },
      usuario: {
        required: true
      },
      clave: {
        required: true,
        minlength: 5
      },
      fechaNacimiento: {
        required: true
      }
    },
    messages: {
      apellido: {
        required : "Ingrese los apellidos"
      },
      nombre: {
        required : "Ingrese los nombres"
      },
      email: {
        required: "Ingrese un correo electrónico",
        email: "El correo es incorrecto"
      },
      usuario: {
        required : "Ingrese un usuario"
      },
      clave: {
        required: "Ingrese una clave",
        minlength: "YMínimo 5 caracteres"
      },
      fechaNacimiento: {
        required: "Ingrese una fecha",
        date: "Fecha incorrecta"
      }  
    },
    errorElement: 'span',
      errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});

</script>   

<?= $this->endSection(); ?>